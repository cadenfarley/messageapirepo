package com.cbsnorthstar.messageapitest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

public class MainActivityMobile extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static GoogleApiClient client;

    public void buttonPressed(View v) {
        String message = "I am a message!";
        //Requires a new thread to avoid blocking the UI
        new SendToDataLayerThread("/message_path", message).start();
        Toast t = Toast.makeText(getApplicationContext(),"Message \"" + message + "\" sent",Toast.LENGTH_SHORT);
        t.show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mobile);

        client = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }
    // Connect to the data layer when the Activity starts
    @Override
    protected void onStart() {
        super.onStart();
        client.connect();
    }

    @Override
    public void onConnected(Bundle connectionHint) {}

    // Disconnect from the data layer when the Activity stops
    @Override
    protected void onStop() {
        if (null != client && client.isConnected()) {
            client.disconnect();
        }
        super.onStop();
    }

    // Placeholders for required connection callbacks
    @Override
    public void onConnectionSuspended(int cause) { }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) { }
}
